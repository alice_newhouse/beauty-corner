Shader "Hidden/Roystan/Outline Post Process"
{
    SubShader
    {
        Cull Off ZWrite Off ZTest Always

        Pass
        {
			// Custom post processing effects are written in HLSL blocks,
			// with lots of macros to aid with platform differences.
			// https://github.com/Unity-Technologies/PostProcessing/wiki/Writing-Custom-Effects#shader
            HLSLPROGRAM
            #pragma vertex VertDefault
            #pragma fragment Frag
			#define TEXTURE2D_FLOAT2D(name) Texture2D_float name; SamplerState sampler##name;

			//#include "Packages/com.unity.postprocessing/PostProcessing/Shaders/StdLib.hlsl"

			TEXTURE2D_SAMPLER2D(_MainTex, sampler_MainTex);
			// _CameraNormalsTexture contains the view space normals transformed
			// to be in the 0...1 range.
			TEXTURE2D_SAMPLER2D(_CameraNormalsTexture, sampler_CameraNormalsTexture);
			TEXTURE2D_SAMPLER2D(_CameraDepthTexture, sampler_CameraDepthTexture);
        
			// Data pertaining to _MainTex's dimensions.
			// https://docs.unity3d.com/Manual/SL-PropertiesInPrograms.html
			float4 _MainTex_TexelSize;

			float _Scale;

			float _DepthThreshold;

			float4x4 _ClipToView;

			float4 _Color;

			float4 Frag(VaryingsDefault i) : SV_Target
			{
				float halfScaleFloor = floor(_Scale * 0.5);
			    float halfScaleCeil = ceil(_Scale * 0.5);

		     	float2 bottomLeftUV = i.texcoord - float2(_MainTex_TexelSize.x, _MainTex_TexelSize.y) * halfScaleFloor;
			    float2 topRightUV = i.texcoord + float2(_MainTex_TexelSize.x, _MainTex_TexelSize.y) * halfScaleCeil;
			    float2 bottomRightUV = i.texcoord + float2(_MainTex_TexelSize.x * halfScaleCeil, -_MainTex_TexelSize.y * halfScaleFloor);
		    	float2 topLeftUV = i.texcoord + float2(-_MainTex_TexelSize.x * halfScaleFloor, _MainTex_TexelSize.y * halfScaleCeil);

				float depth0 = SAMPLE_DEPTH_TEXTURE(_CameraDepthTexture, sampler_CameraDepthTexture, bottomLeftUV).r;
				float depth1 = SAMPLE_DEPTH_TEXTURE(_CameraDepthTexture, sampler_CameraDepthTexture, topRightUV).r;
				float depth2 = SAMPLE_DEPTH_TEXTURE(_CameraDepthTexture, sampler_CameraDepthTexture, bottomRightUV).r;
				float depth3 = SAMPLE_DEPTH_TEXTURE(_CameraDepthTexture, sampler_CameraDepthTexture, topLeftUV).r;

				float4 colour = SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, i.texcoord);
				

				float depthFiniteDifference0 = depth1 - depth0;
				float depthFiniteDifference1 = depth3 - depth2;

				float depthVariance = sqrt(pow(depthFiniteDifference0, 2) + pow(depthFiniteDifference1, 2)) * 100;

				if (depthVariance > 0.5)	//This pixel is an outline!
				{
					return float4(0.0, 0.0, 0.0, 1.0);
				}
				else	//This pixel isn't an outline, it's just a normal pixel.
				{
					return colour;
				}
				
				
				
				/* This is some extra depth stuff but we needn't worry about it for now.
				float depthThreshold = _DepthThreshold * depth0;
				depthVariance = depthVariance > depthThreshold ? 1 : 0;
				depthVariance = depthVariance > _DepthThreshold ? 1 : 0;
				
				return float4(depthVariance, depthVariance, depthVariance, 1.0);*/
			}
			ENDHLSL
		}
    }
}